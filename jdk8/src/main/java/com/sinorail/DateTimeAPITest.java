package com.sinorail;

import org.junit.Test;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/21 16:15
 */
public class DateTimeAPITest {

    @Test
    public void test() {
        /*LocalDate 获取本地日期*/
        LocalDate localDate;
        /* 获得 2019 年的第 200 天 */
        localDate = LocalDate.ofYearDay(2019, 200);
        System.out.println(localDate.toString());
        localDate = LocalDate.of(2019, Month.SEPTEMBER, 10);
        System.out.println(localDate.toString());

        /*LocalTime*/
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime.toString());
        localTime = LocalTime.of(10, 20, 50);
        System.out.println(localTime.toString());

        /*Clock 时钟*/
        Clock clock = Clock.systemDefaultZone();
        long millis = clock.millis();
        System.out.println(millis);

        /*解析*/
        final LocalDate date = LocalDate.parse("20180920", DateTimeFormatter.BASIC_ISO_DATE);
        System.out.println(date.toString());
    }
}

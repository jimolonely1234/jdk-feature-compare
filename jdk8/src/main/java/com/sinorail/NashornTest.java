package com.sinorail;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * @author wangpeng
 * @func js引擎测试
 * @date 2018/8/21 16:28
 */
public class NashornTest {
    public static void main(String[] args) {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");

        String name = "jimo";
        Integer result;

        try {
            nashorn.eval("print('" + name + "')");
            result = (Integer) nashorn.eval("13 + 14");
            System.out.println(result);
        } catch (ScriptException e) {
            System.out.println("执行脚本错误: " + e.getMessage());
        }
    }
}

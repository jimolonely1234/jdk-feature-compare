package com.sinorail;

/**
 * @author wangpeng
 * @func lambda表达式演示
 * @date 2018/8/21 8:59
 */
public class LambdaTest {

    interface MyActionListener {
        /**
         * 回调方法
         */
        void doSomething(String msg);
    }

    class MyButton {
        String msg = "";

        public MyButton(String msg) {
            this.msg = msg;
        }

        void click(MyActionListener listener) {
            listener.doSomething(msg);
        }
    }

    public void lambda() {
        MyButton button = new MyButton("hehe");

        button.click(new MyActionListener() {
            @Override
            public void doSomething(String msg) {
                System.out.println(msg);
            }
        });

        button.click((msg) -> System.out.println(msg));
    }

    public void lambdaThread() {
        new Thread(() -> {
            System.out.println("run");
        });
    }

    /**
     * @func 方法引用和lambda
     * @author wangpeng
     * @date 2018/8/21 9:20
     */
    public void lambdaVSReference() {
        //1.
        MyActionListener listener = new MyActionListener() {

            @Override
            public void doSomething(String msg) {
                System.out.println(msg);
            }
        };

        //2.lambda
        MyActionListener listener1 = (msg) -> System.out.println(msg);

        //3.方法引用
        MyActionListener listener2 = System.out::println;
    }


}

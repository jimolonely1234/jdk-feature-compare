package com.sinorail;

import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * @author wangpeng
 * @func https://docs.oracle.com/javase/tutorial/essential/io/file.html
 * @date 2018/8/21 16:00
 */
public class IOTest {

    private final String path = "E:\\work-doc\\hosts";
    private final String pathDir = "E:\\work-doc";

    /**
     * @func bufferedReader.lines
     * @author wangpeng
     * @date 2018/8/21 15:51
     */
    @Test
    public void bufferedReaderLines() throws FileNotFoundException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            final Stream<String> lines = bufferedReader.lines();
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @func Files.lines
     * @author wangpeng
     * @date 2018/8/21 15:54
     */
    @Test
    public void filesLines() {
        try (final Stream<String> lines = Files.lines(Paths.get(path))) {
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/21 16:07
     */
    @Test
    public void filesList() {
        try (final Stream<Path> lines = Files.list(Paths.get(pathDir))) {
            lines.forEach(p -> System.out.println(p.getFileName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

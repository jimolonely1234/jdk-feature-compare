package com.sinorail;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

/**
 * @author wangpeng
 * @func 流式集合测试
 * @date 2018/8/21 9:42
 */
public class StreamCollectionTest {

    private List<Integer> getRandomList() {
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10000000; i++) {
            list.add(random.nextInt(1000));
        }
        return list;
    }

    /**
     * @func 串行排序
     * @author wangpeng
     * @date 2018/8/21 9:46
     */
    @Test
    public void serialSort() {
        long start = System.nanoTime();
        getRandomList().stream().sequential().sorted().count();
        long end = System.nanoTime();
        long ms = TimeUnit.NANOSECONDS.toMillis(end - start);
        System.out.println("sequential time:" + ms);
        /*6612ms*/
    }

    /**
     * @func 并行排序
     * @author wangpeng
     * @date 2018/8/21 9:46
     */
    @Test
    public void parallelSort() {
        long start = System.nanoTime();
        getRandomList().stream().parallel().sorted().count();
        long end = System.nanoTime();
        long ms = TimeUnit.NANOSECONDS.toMillis(end - start);
        System.out.println("parallel time:" + ms);
        /*4760ms*/
    }

    /**
     * @func 中间操作
     * @author wangpeng
     * @date 2018/8/21 9:56
     */
    @Test
    public void intermediateOperation() {
        List<Integer> list = getRandomList();
        Stream<Integer> sortedStream = list.stream()
                .filter(x -> x > 100)
                .map(x -> x * 2)
                .distinct()
                .sorted();
        System.out.println(sortedStream.count());
    }

    /**
     * @func 终止操作
     * @author wangpeng
     * @date 2018/8/21 9:57
     */
    @Test
    public void terminateOperation() {
        List<Integer> list = getRandomList();
        Stream<Integer> stream = list.stream().distinct();

        stream.forEach(System.out::print);
        /*stream已经关闭，下面的操作无法进行*/

        Object[] array = stream.toArray();
        Optional<Integer> first = stream.findFirst();
        boolean anyMatch = stream.anyMatch(x -> x > 200);
        System.out.println(anyMatch);
    }
}

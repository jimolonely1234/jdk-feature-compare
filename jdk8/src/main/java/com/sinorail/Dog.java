package com.sinorail;

import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author wangpeng
 * @func 方法引用测试
 * @date 2018/8/21 15:10
 */
public class Dog {
    public static Dog newDog(final Supplier<Dog> supplier) {
        return supplier.get();
    }

    public static void wow(final Dog dog) {
        System.out.println("汪：" + dog.toString());
    }

    public void play(final Dog dog) {
        System.out.println("和另一只狗玩耍：" + dog.toString());
    }

    public void die() {
        System.out.println("狗死了：" + this.toString());
    }


    public static void main(String[] args) {
        final Dog dog = Dog.newDog(Dog::new);
        final Consumer<Dog> die = Dog::die;
        final BiConsumer<Dog, Dog> play = Dog::play;
        final Consumer<Dog> dogConsumer = dog::play;
        final Consumer<Dog> wow = Dog::wow;

        List<Dog> dogs = Collections.singletonList(dog);

        dogs.forEach(die);

        dogs.forEach(wow);

        dogs.forEach(dog::play);
    }
}

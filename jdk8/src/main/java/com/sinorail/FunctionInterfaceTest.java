package com.sinorail;

import org.junit.Test;

/**
 * @author wangpeng
 * @func 函数式接口
 * @date 2018/8/21 9:24
 */
public class FunctionInterfaceTest {

    @FunctionalInterface
    interface MyFuncInterface {
        abstract void run();

//        void hehe(String msg);

        default void defaultMethod() {
            System.out.println("default method");
        }

        static void staticMethod() {
            System.out.println("static method");
        }

        @Override
        boolean equals(Object obj);
    }

    @Test
    public void test() {
        MyFuncInterface funcInterface = () -> System.out.println("run");
        funcInterface.run();
        funcInterface.defaultMethod();
        MyFuncInterface.staticMethod();
    }
}

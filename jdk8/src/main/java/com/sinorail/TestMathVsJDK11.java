package com.sinorail;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author wangpeng
 * @func 测试Math函数的时间, 看JDK11在优化了64位架构后有什么提升
 * @date 2018/8/25 19:16
 */
public class TestMathVsJDK11 {

    /**
     * @func 测试
     * @author wangpeng
     * @date 2018/8/25 19:17
     */
    @Test
    public void testMathOnJDK8() {
        final long startTime = System.nanoTime();
        for (int i = 0; i < 10000000; i++) {
            Math.sin(i);
            Math.cos(i);
            Math.log(i);
        }
        final long endTime = System.nanoTime();
        System.out.println(TimeUnit.NANOSECONDS.toMillis(endTime - startTime) + "ms");
        /*8700ms ,8724ms*/
    }
}

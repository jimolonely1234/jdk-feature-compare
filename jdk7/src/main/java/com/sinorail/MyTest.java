package com.sinorail;


import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class MyTest {

    public void method(String... args) {
        out.println();
    }

    /**
     * @func try-with-resource语句测试
     * https://docs.oracle.com/javase/7/docs/technotes/guides/language/try-with-resources.html
     * @author wangpeng
     * @date 2018/8/20 20:53
     */
    @Test
    public void tryWith() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("path"))) {
            br.readLine();
        }
    }

    /**
     * @func https://docs.oracle.com/javase/7/docs/technotes/guides/language/binary-literals.html
     * @author wangpeng
     * @date 2018/8/20 21:00
     */
    @Test
    public void binary() {
        // An 8-bit 'byte' value:
        byte aByte = (byte) 0b00100001;

        // A 16-bit 'short' value:
        short aShort = (short) 0b1010000101000101;

        // Some 32-bit 'int' values:
        int anInt1 = 0b10100001010001011010000101000101;
        int anInt2 = 0b101;
        int anInt3 = 0B101;

        System.out.println(anInt1 + "," + anInt2);

        // A 64-bit 'long' value. Note the "L" suffix:
        long aLong = 0b1010000101000101101000010100010110100001010001011010000101000101L;

        int hex = 0xaf;
    }

    /**
     * @func 在数字的开头或结尾
     * 与浮点数的小数点相邻
     * 在F或L后缀之前
     * 预期有一串数字的位置
     * https://docs.oracle.com/javase/7/docs/technotes/guides/language/underscores-literals.html
     * @author wangpeng
     * @date 2018/8/20 21:06
     */
    @Test
    public void underScore() {
        long creditCardNumber = 1234_5678_9012_3456L;
        long socialSecurityNumber = 999_99_9999L;
        float pi = 3.14_15F;
        long hexBytes = 0xFF_EC_DE_5E;
        long hexWords = 0xCAFE_BABE;
        long maxLong = 0x7fff_ffff_ffff_ffffL;
        byte nybbles = 0b0010_0101;
        long bytes = 0b11010010_01101001_10010100_10010010;
    }

    @Test
    public void collection() {
        List<String> list = new ArrayList<>();
//        List<String> s = ["heh","haha"];
    }

    /**
     * @func https://docs.oracle.com/javase/7/docs/technotes/guides/language/catch-multiple.html
     * @author wangpeng
     * @date 2018/8/20 21:15
     */
    public void multiException() {
        boolean ok = false;
        try {
            if (ok) {
                throw new IOException();
            } else {
                throw new SQLException();
            }
        } catch (IOException | SQLException ex) {
        }
    }


}

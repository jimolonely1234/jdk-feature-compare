package com.sinorail;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;

/**
 * @author wangpeng
 * @func 变量句柄测试
 * @date 2018/8/22 9:11
 */
public class VarHandleTest {
    class HandleTarget {
        public int count = 1;
    }

    private HandleTarget target;
    private VarHandle varHandle;

    @Before
    public void init() throws NoSuchFieldException, IllegalAccessException {
        target = new HandleTarget();
        varHandle = MethodHandles.lookup().findVarHandle(HandleTarget.class, "count", int.class);
    }

    /**
     * @func 测试各种获取操作
     * @author wangpeng
     * @date 2018/8/22 9:15
     */
    @Test
    public void testGet() {
        assertEquals(1, varHandle.get(target));
        assertEquals(1, varHandle.getVolatile(target));
        assertEquals(1, varHandle.getOpaque(target));
        assertEquals(1, varHandle.getAcquire(target));
    }



}

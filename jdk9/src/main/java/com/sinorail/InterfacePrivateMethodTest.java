package com.sinorail;

/**
 * @author wangpeng
 * @func 接口私有（静态）方法测试
 * @date 2018/8/22 9:40
 */
public interface InterfacePrivateMethodTest {
    private void privateMethod() {
        System.out.println("hello private");
    }

    private static void privateStaticMethod() {
        System.out.println("hello private static");
    }

    default void test() {
        privateStaticMethod();
        privateMethod();
    }

    static void main(String[] args) {
        InterfacePrivateMethodTest.privateStaticMethod();
        InterfacePrivateMethodTest obj = new InterfacePrivateMethodTest() {
        };
        obj.test();
    }
}

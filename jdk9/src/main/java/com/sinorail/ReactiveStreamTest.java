package com.sinorail;

import org.junit.Test;

import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/28 19:21
 */
public class ReactiveStreamTest {

    class PrintSubscriber implements Flow.Subscriber<Integer> {
        private Flow.Subscription subscription;

        @Override
        public void onSubscribe(Flow.Subscription subscription) {
            this.subscription = subscription;
            this.subscription.request(1);
        }

        @Override
        public void onNext(Integer item) {
            System.out.println("接收到：" + item);
            subscription.request(1);
            /*1代表请求的步长,表示再请求1个,<=0时跳到error*/
        }

        @Override
        public void onError(Throwable throwable) {
            System.out.println("error:" + throwable.getMessage());
        }

        @Override
        public void onComplete() {
            System.out.println("订阅完毕");
        }
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/28 19:23
     */
    @Test
    public void subscribe() throws InterruptedException {
        SubmissionPublisher<Integer> publisher = new SubmissionPublisher<>();
        publisher.subscribe(new PrintSubscriber());
        System.out.println("开始订阅");
        for (int i = 0; i < 10; i++) {
            publisher.submit(i);
        }
        Thread.sleep(1000);
        publisher.close();
    }
}

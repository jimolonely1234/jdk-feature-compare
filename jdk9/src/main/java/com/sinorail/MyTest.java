package com.sinorail;

import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.logging.*;

import static org.junit.Assert.assertEquals;

public class MyTest {
    /**
     * @func 不可变集合测试
     * @author wangpeng
     * @date 2018/8/21 20:59
     */
    @org.junit.Test
    public void testCollection() {
        final List<String> list = List.of("hello", "jimo");
        list.forEach(System.out::println);
        final Set<Integer> set = Set.of(1, 2, 3, 1, 2, 3);
        final Map<String, Integer> map = Map.of("a", 1, "b", 2);
    }

    /**
     * @func takeWhile, dropWhile
     * @author wangpeng
     * @date 2018/8/21 21:27
     */
    @org.junit.Test
    public void testStream() {
        /*遇到第一个不满足则跳出*/
        final long count = Stream.of(1, 2, 3, 4, 5)
                .sorted()
                .dropWhile(i -> i % 2 != 0)
                .count();
        assertEquals(4, count);

        final long count1 = Stream.of(1, 2, 3, 4, 5)
                .sorted()
                .takeWhile(i -> i > 3)
                .count();
        assertEquals(0, count1);
    }

    /**
     * @func Collections新增方法
     * @author wangpeng
     * @date 2018/8/21 21:26
     */
    @org.junit.Test
    public void collectionTest() {
        final Set<Integer> collect = Stream.of("a", "b", "c", "b", "a", "d")
                .collect(Collectors.flatMapping(v -> v.chars().boxed(), Collectors.toSet()));
        assertEquals(4, collect.size());

        final List<Integer> list = Stream.of(1, 2, 3, 4, 3, 2, 5).collect(
                Collectors.filtering(v -> v > 2, Collectors.toList()));
        assertEquals(4, list.size());
    }

    /**
     * @func Optional新增方法：ifPresentOrElse、or 和 stream等
     * @author wangpeng
     * @date 2018/8/21 21:37
     */
    @org.junit.Test
    public void testOptional() {
        final Optional<Object> empty = Optional.empty();
        final Optional<Integer> integer = Optional.of(1);

        integer.ifPresentOrElse(i -> i++, () -> System.out.println("not present"));
        empty.ifPresentOrElse(System.out::println, () -> System.out.println("empty not present"));

        final Optional<Integer> integer1 = integer.or(() -> Optional.of(0));
        System.out.println(integer1.get());

        Stream.of(
                Optional.of(1),
                Optional.empty(),
                Optional.of(2)
        ).flatMap(Optional::stream).forEach(System.out::println);
    }


    /**
     * @func 新增ProcessHandle 接口，可以对原生进程进行管理，尤其适合于管理长时间运行的进程
     * @author wangpeng
     * @date 2018/8/22 8:12
     */
    @Test
    public void testProcess() throws IOException {
        /*运行一条命令*/
        final ProcessBuilder builder = new ProcessBuilder("java", "-version").inheritIO();
        final ProcessHandle processHandle = builder.start().toHandle();
        processHandle.onExit().whenCompleteAsync((handle, throwable) -> {
            if (throwable == null) {
                System.out.println(handle.pid());
            } else {
                throwable.printStackTrace();
            }
        });
    }

    /**
     * @func 新增的Logger，LoggerFinder可用来管理使用其他日志框架
     * @author wangpeng
     * @date 2018/8/22 8:29
     */
    @Test
    public void testLogger() {
        final System.Logger logger = System.getLogger("JDK9 Test");
        logger.log(System.Logger.Level.INFO, "hahahaha");

        final Logger logger1 = Logger.getLogger("Util-Logger");
        logger1.log(Level.INFO, "debug util");

        final System.LoggerFinder loggerFinder = System.LoggerFinder.getLoggerFinder();
        final System.Logger findLogger = loggerFinder.getLogger(logger1.getName(), logger1.getClass().getModule());
        findLogger.log(System.Logger.Level.ERROR, "find logger");
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/22 8:48
     */
    @Test
    public void testReactiveStream() {
        //TODO
    }
}

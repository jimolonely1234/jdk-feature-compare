package com.sinorail;

import org.junit.Test;

import java.beans.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author wangpeng
 * @func 内省测试
 * @date 2018/8/21 7:55
 */
public class IntrospectorTest {

    /**
     * @func 获取方法描述
     * @author wangpeng
     * @date 2018/8/21 8:06
     */
    @Test
    public void getMethodDesc() throws IntrospectionException {
        User user = new User();
        BeanInfo bean = Introspector.getBeanInfo(user.getClass());
        MethodDescriptor[] methodDescriptors = bean.getMethodDescriptors();
        for (MethodDescriptor descriptor : methodDescriptors) {
            Method method = descriptor.getMethod();
            String methodName = method.getName();
            System.out.print(methodName + "(");
            Class<?>[] parameterTypes = method.getParameterTypes();
            for (Class<?> type : parameterTypes) {
                System.out.print(type.getSimpleName() + ",");
            }
            System.out.println(")");
        }
    }

    /**
     * @func 获取类属性值
     * @author wangpeng
     * @date 2018/8/21 8:18
     */
    @Test
    public void getProperties() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        User user = new User();
        user.setAge(100);
        user.setName("hehe");

        BeanInfo beanInfo = Introspector.getBeanInfo(user.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        if (propertyDescriptors != null) {
            for (PropertyDescriptor descriptor : propertyDescriptors) {
                Method readMethod = descriptor.getReadMethod();
                Object invoke = readMethod.invoke(user);
                System.out.println(invoke.toString());
            }
        }
    }

    /**
     * @func 修改类的属性值
     * @author wangpeng
     * @date 2018/8/21 8:23
     */
    @Test
    public void setProperties() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        User user = new User();
        user.setAge(100);
        user.setName("hehe");

        BeanInfo beanInfo = Introspector.getBeanInfo(user.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        if (propertyDescriptors != null) {
            for (PropertyDescriptor descriptor : propertyDescriptors) {
                if ("name".equals(descriptor.getName())) {
                    Method writeMethod = descriptor.getWriteMethod();
                    writeMethod.invoke(user, "jimo");
                    break;
                }
            }
        }
        System.out.println(user);
    }

    /**
     * @func 使用反射对比
     * @author wangpeng
     * @date 2018/8/21 8:34
     */
    @Test
    public void reflect() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> userClass = Class.forName("com.sinorail.User");
        Method[] methods = userClass.getMethods();
        for (Method method : methods) {
            System.out.println(method.getName());
        }
    }
}

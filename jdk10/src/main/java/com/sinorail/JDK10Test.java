package com.sinorail;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Assert.*;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author wangpeng
 * @func JDK10
 * @date 2018/8/25 15:17
 */
public class JDK10Test {

    /**
     * @func 变量推断验证
     * @author wangpeng
     * @date 2018/8/25 15:18
     */
    @Test
    public void varTest() {
        var list = new ArrayList<String>();
        System.out.println(list.getClass());
        /*class java.util.ArrayList*/

        for (var i = 0; i < 10; i++) {
            System.out.println(i);
        }

        var list2 = getList();
        System.out.println(list2.getClass());
        /*class java.util.ArrayList*/


        String message = "";
        Function<String, String>
                quotify = m -> "'" + message + "'";

//        var quotify = m -> "'" + message + "'";
    }

    private List<String> getList() {
        return new ArrayList<>();
    }


    /**
     * @func
     * @author wangpeng
     * @date 2018/8/25 16:35
     */
    @Test
    public void unicodeTest() {
        final Currency instance = Currency.getInstance(Locale.CHINA);
        System.out.println(instance.getDisplayName());
        /*人民币*/

        final Calendar calendar = Calendar.getInstance();
        final int firstDayOfWeek = calendar.getFirstDayOfWeek();
        System.out.println(firstDayOfWeek);
        Assert.assertEquals(firstDayOfWeek, Calendar.SUNDAY);

        final DateTimeFormatter dateTime = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        final DateTimeFormatter japanDateTime = dateTime.localizedBy(Locale.JAPAN);
        System.out.println(japanDateTime.toFormat());
    }
}

package com.sinorail;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author wangpeng
 * @func lambda var参数测试
 * @date 2018/8/25 22:53
 */
public class LambdaParamTest {

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/25 22:53
     */
    @Test
    public void testParam() {
        final long count = Arrays
                .stream(new int[]{1, 2, 3, 4})
                .filter((@Nonnull var x) -> x > 2)
                .count();
        System.out.println(count);
    }
}

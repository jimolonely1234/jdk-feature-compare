package com.sinorail;

import org.junit.Test;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.net.Socket;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/26 20:06
 */
public class UseTLS {

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/26 20:06
     */
    @Test
    public void useTLS() throws IOException {
        String host = "www.baidu.com";
        int port = 443;
        SocketFactory basicSocketFactory = SocketFactory.getDefault();
        Socket s = basicSocketFactory.createSocket(host, port);
        /*s 是一个 TCP socket*/
        SSLSocketFactory tlsSocketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        s = tlsSocketFactory.createSocket(s, host, port, true);
        /*s 现在是一个 基于TCP的TLS socket*/
    }
}

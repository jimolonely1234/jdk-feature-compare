package com.sinorail;

import org.junit.Test;

import javax.crypto.KeyAgreement;
import javax.script.ScriptEngineManager;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.NamedParameterSpec;
import java.security.spec.XECPublicKeySpec;
import java.util.Arrays;

/**
 * @author wangpeng
 * @func 安全协议API使用
 * @date 2018/8/25 23:02
 */
public class KeyAgreementTest {

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/25 23:03
     */
    @Test
    public void keyAgreementTest() throws NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeySpecException, InvalidKeyException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("XDH");
        NamedParameterSpec paramSpec = new NamedParameterSpec("X25519");
        kpg.initialize(paramSpec);
        KeyPair kp = kpg.generateKeyPair();

        KeyFactory kf = KeyFactory.getInstance("XDH");
        BigInteger u = new BigInteger("1212324324");
        XECPublicKeySpec pubSpec = new XECPublicKeySpec(paramSpec, u);
        PublicKey pubKey = kf.generatePublic(pubSpec);

        KeyAgreement ka = KeyAgreement.getInstance("XDH");
        ka.init(kp.getPrivate());
        ka.doPhase(pubKey, true);
        byte[] secret = ka.generateSecret();
        System.out.println(Arrays.toString(secret));

//        new ScriptEngineManager().getEngineByName("nashorn");
    }
}

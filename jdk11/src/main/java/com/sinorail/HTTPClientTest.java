package com.sinorail;

import org.junit.Test;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/25 22:29
 */
public class HTTPClientTest {

    /**
     * @func get请求
     * @author wangpeng
     * @date 2018/8/25 22:30
     */
    @Test
    public void testGet() {
        final HttpClient client = HttpClient.newHttpClient();
        final HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://openjdk.java.net"))
                .build();
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenAccept(System.out::println)
                .join();
    }
}

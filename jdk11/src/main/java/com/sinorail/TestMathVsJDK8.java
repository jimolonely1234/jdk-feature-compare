package com.sinorail;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/25 19:24
 */
public class TestMathVsJDK8 {
    /**
     * @func 测试Math函数的时间, 看JDK11在优化了64位架构后有什么提升
     * @author wangpeng
     * @date 2018/8/25 19:24
     */
    @Test
    public void testMathOnJDK11() {
        final long startTime = System.nanoTime();
        for (int i = 0; i < 10000000; i++) {
            Math.sin(i);
            Math.cos(i);
            Math.log(i);
        }
        final long endTime = System.nanoTime();
        System.out.println(TimeUnit.NANOSECONDS.toMillis(endTime - startTime) + "ms");
        /*1279ms,1477ms*/
    }
}

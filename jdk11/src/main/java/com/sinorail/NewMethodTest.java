package com.sinorail;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author wangpeng
 * @func JDK11新增方法测试
 * @date 2018/8/26 22:36
 */
public class NewMethodTest {

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/26 22:37
     */
    @Test
    public void testString() {
        String s = " Jimo ";
        Assert.assertEquals("Jimo", s.strip());
        Assert.assertEquals("Jimo ", s.stripLeading());
        Assert.assertEquals(" Jimo", s.stripTrailing());
        Assert.assertFalse(s.isBlank());
        Assert.assertEquals(" Jimo  Jimo  Jimo ", s.repeat(3));

        var lines = "I'm a super man\ndo you know me?\n";
        final Stream<String> stringStream = lines.lines();
        System.out.println(stringStream.collect(Collectors.toList()));
        /*[I'm a super man, do you know me?]*/
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/26 22:46
     */
    @Test
    public void testFile() throws IOException {
        final Path path = Path.of("test.txt");
        Files.writeString(path, "Ha jimo");
        Assert.assertEquals("Ha jimo", Files.readString(path));

        Assert.assertTrue(Files.isSameFile(path, path));
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/27 8:24
     */
    @Test
    public void testPredicate() {
        final long count = Stream.of("a", "b", "")
                .filter(Predicate.not(String::isEmpty))
                .count();
        Assert.assertEquals(2, count);

        final Predicate<String> predicate =
                Pattern.compile("jimo.").asMatchPredicate();
        Assert.assertTrue(predicate.test("jimo1"));
    }

    /**
     * @func Thread移除stop和destroy方法
     * @author wangpeng
     * @date 2018/8/27 8:37
     */
    @Test
    public void testThread() {
        final Thread t = new Thread();
        t.stop();
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/27 8:40
     */
    @Test
    public void testTimeUnit() {
        final TimeUnit unit = TimeUnit.DAYS;
        Assert.assertEquals(2, unit.convert(Duration.ofHours(49)));
        Assert.assertEquals(1, unit.convert(Duration.ofMinutes(1450)));
    }
}
